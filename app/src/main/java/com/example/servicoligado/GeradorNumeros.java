package com.example.servicoligado;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GeradorNumeros extends Service {
    GeradorBinder binder;

    private int intervalo = 2000;
    private LinkedList<Integer> numeros;

    class Gerador extends Thread {
        public void run() {
            Random rand = new Random(System.currentTimeMillis());

            while(true) {
                numeros.add(new Integer(rand.nextInt()));
                System.out.println(numeros);
                try{
                    Thread.sleep(intervalo);
                }catch (Throwable t) {

                }
            }
        }
    }

    public GeradorNumeros() {
        binder = new GeradorBinder();
        numeros = new LinkedList<>();
    }

    public class GeradorBinder extends Binder {
        public GeradorNumeros getService() {
            return GeradorNumeros.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void iniciarGeracao() {
        new Gerador().start();
    }

    public List<Integer> consultar() {
        LinkedList<Integer> resp = new LinkedList<>(numeros);
        numeros.clear();
        return resp;
    }

    public void maisRapido() {
        if(intervalo > 100) {
            intervalo = (int) (intervalo * 0.66666);
        }
    }

    public void maisLento() {
        intervalo = (int) (intervalo * 1.5);
    }
}