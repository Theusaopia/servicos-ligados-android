package com.example.servicoligado;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    TextView textoConsulta;

    GeradorNumeros servico;
    ServiceConnection conexao = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            servico = ((GeradorNumeros.GeradorBinder) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            servico = null;
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Intent it = new Intent(getApplicationContext(), GeradorNumeros.class);
        bindService(it, conexao, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        unbindService(conexao);
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textoConsulta = (TextView) findViewById(R.id.textoConsulta);
    }

    public void iniciar(View view) {
        if(servico != null) {
            servico.iniciarGeracao();
        }
    }

    public void consultar(View view) {
        if(servico != null) {
            List<Integer> lst = servico.consultar();

            if(lst == null || lst.isEmpty()) {
                textoConsulta.setText("Não há o que exibir");
            }else {
                textoConsulta.setText("");

                for(Integer val : lst) {
                    textoConsulta.append(String.valueOf(val) + "\n");
                }
            }
        }
    }

    public void acelerar(View view) {
        if(servico != null) {
            servico.maisRapido();
        }
    }

    public void desacelerar(View view) {
        if(servico != null) {
            servico.maisLento();
        }
    }
}